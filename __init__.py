# -*- coding: utf-8 -*-
# This file is part of health_stock_hesm module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from . import health
from . import shipment
from .wizard import *


def register():
    Pool.register(
        health.PatientPrescriptionOrder,
        health.Medicament,
        health.HealthInstitution,
        health.PrescriptionLine,
        wizard_health_stock_create_prescription.CreatePrescriptionAndStockMoveStart,
        wizard_health_stock_create_prescription.CreatePrescriptionAndStockMoveStartLine,
        shipment.ShipmentIn,
        module='health_stock_hesm', type_='model')
    Pool.register(
        wizard_health_stock_create_prescription.CreatePrescriptionAndStockMove,
        wizard_health_stock_make_stock_move.CreateStockMove,
        module='health_stock_hesm', type_='wizard')
