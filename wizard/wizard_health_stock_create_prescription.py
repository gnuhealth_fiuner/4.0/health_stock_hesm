# -*- coding: utf-8 -*-
# This file is part of health_stock_hesm module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from datetime import datetime, timedelta

from trytond.model import ModelView, fields
from trytond.wizard import (Wizard, StateView, Button, StateTransition,
    StateAction)
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Or, PYSONEncoder, Not
from trytond.transaction import Transaction
from trytond.exceptions import UserWarning
from trytond.modules.health.core import get_institution

__all__ = ['CreatePrescriptionAndStockMoveStart',
    'CreatePrescriptionAndStockMoveStartLine',
    'CreatePrescriptionAndStockMove']


class CreatePrescriptionAndStockMoveStart(ModelView):
    'Create Prescription Order and Stock Move'
    __name__ = 'gnuhealth.prescription.create.start'

    patient = fields.Many2One('gnuhealth.patient', 'Patient', required=True)
    prescription_type = fields.Selection([
        ('inpatient', 'Inpatient'),
        ('outpatient', 'Outpatient'),
        ], 'Type', sort=False)
    has_insurance = fields.Boolean('Has Insurance')
    prescription_date = fields.DateTime('Prescription Date', required=True)
    healthprof = fields.Many2One('gnuhealth.healthprofessional',
        'Prescribed by', required=True)
    diagnosis = fields.Many2One('gnuhealth.pathology', 'Diagnosis')
    pregnancy_warning = fields.Boolean('Pregnancy Warning', readonly=True)
    pharmacovigilance_warning = fields.Boolean('Pharmacovigilance Warning',
        readonly=True)
    prescription_warning_ack = fields.Boolean('Prescription verified',
        states={'required': Or(
            Bool(Eval('pregnancy_warning', False)),
            Bool(Eval('pharmacovigilance_warning', False)))},
        depends=['pregnancy_warning', 'pharmacovigilance_warning'])
    add_to_history = fields.Boolean(
        'Hist',
        help='Include this medication in the patient medication history')
    notes = fields.Text('Prescription Notes')
    pharmacy = fields.Many2One('party.party', 'Pharmacy',
        domain=[('is_pharmacy', '=', True)], required=True)
    output_location = fields.Many2One('stock.location',
        'Output Location', domain=[('type', '=', 'storage')])
    from_location = fields.Function(fields.Many2One('stock.location',
        'From Location'), 'on_change_with_from_location')
    to_location = fields.Function(fields.Many2One('stock.location',
        'To Location'), 'on_change_with_to_location')
    effective_date = fields.Date('Effective Date', required=True)
    lines = fields.One2Many('gnuhealth.prescription.create.start.line',
        'prescription_create_start', 'Prescription line',
        context={
            'from_location': Eval('from_location'),
            'to_location': Eval('to_location'),
            },
        states={
            'readonly': Not(Bool(Eval('patient'))),
            'required': Bool(Eval('patient')),
            },
        depends=['from_location', 'to_location'])
    prescription = fields.Many2One('gnuhealth.prescription.order',
        'Prescription', )

    prescription_id = fields.Many2One('gnuhealth.prescription.order','Prescription Order')

    @staticmethod
    def default_prescription_type():
        return 'inpatient'

    @staticmethod
    def default_has_insurance():
        return False

    @staticmethod
    def default_prescription_date():
        return datetime.now()

    @staticmethod
    def default_effective_date():
        return datetime.today().date()

    @staticmethod
    def default_add_to_history():
        return True

    @staticmethod
    def default_pharmacy():
        pool = Pool()
        HealthInst = pool.get('gnuhealth.institution')
        pharmacy = None
        if get_institution():
            hi, = HealthInst.search([('id','=', get_institution())])
            if hi.main_pharmacy:
                pharmacy = hi.main_pharmacy.id
        return pharmacy

    @fields.depends('pharmacy')
    def on_change_with_from_location(self, name=None):
        from_location = None
        if self.pharmacy:
            from_location = self.pharmacy.warehouse
            if from_location.type == 'warehouse':
                from_location = from_location.storage_location
        return from_location and from_location.id or None

    @fields.depends('patient')
    def on_change_with_to_location(self):
        to_location = False
        if self.patient:
            to_location = self.patient.name.customer_location
        return to_location and to_location.id or None

    @fields.depends('patient')
    def on_change_with_pregnancy_warning(self):
        # Trigger the warning if the patient is at a childbearing age
        if self.patient and self.patient.childbearing_age:
            return True
        return False

    @fields.depends('pregnancy_warning')
    def on_change_pregnancy_warning(self):
        if self.pregnancy_warning:
            self.prescription_warning_ack = False

    @fields.depends('lines')
    def on_change_with_pharmacovigilance_warning(self):
        # Trigger the warning if any medicament requires pharmacovigilance
        if self.lines:
            for line in self.lines:
                if (line.medicament and
                        line.medicament.pharmacovigilance_warning):
                    return True
        return False

    @fields.depends('pharmacovigilance_warning')
    def on_change_pharmacovigilance_warning(self):
        if self.pharmacovigilance_warning:
            self.prescription_warning_ack = False

    @fields.depends('patient')
    def on_change_with_has_insurance(self):
        if self.patient:
            return True if self.patient.current_insurance else False


class CreatePrescriptionAndStockMoveStartLine(ModelView):
    'Prescription Line'
    __name__ = 'gnuhealth.prescription.create.start.line'

    prescription_create_start = fields.Many2One(
        'gnuhealth.prescription.create.start','Prescription Create Start')

    medicament = fields.Many2One('gnuhealth.medicament', 'Medicament',
        required=True)
    product = fields.Many2One('product.product', 'Product')
    lot_required = fields.Boolean('Lot required')
    lot = fields.Many2One('stock.lot', 'Lot',
        domain=[('product', '=', Eval('product'))],
        states={'required': Bool(Eval('lot_required'))},
        context={
            'locations': [Eval('context', {}).get('from_location', None)],
            },
        depends=['product', 'lot_required'])
    quantity = fields.Integer('Quantity', required=True)

    end_prescription_date = fields.DateTime('Prescription End Date', required=True)

    qty_in_location = fields.Char('Quantity in pharmacy')

    lot_exp_date = fields.Date('Expiration Date')

    is_pharma_warning = fields.Boolean('Pharmacovigilance')

    @fields.depends('medicament')
    def on_change_with_is_pharma_warning(self):
        if self.medicament and self.medicament.pharmacovigilance_warning:
            return True
        return False

    @staticmethod
    def default_end_prescription_date():
        return datetime.now()+timedelta(days=14)

    @fields.depends('medicament')
    def on_change_with_qty_in_location(self):
        #if self.medicament:
            #return self.medicament.quantity if self.medicament.quantity>0 else 'Cantidad indefinida'
        return 'Cantidad indefinida'

    @fields.depends('lot')
    def on_change_with_lot_exp_date(self):
        if self.lot:
            return self.lot.expiration_date
        return None

    @fields.depends('medicament')
    def on_change_medicament(self):
        context = Transaction().context
        pool = Pool()
        Location = pool.get('stock.location')

        product = None
        lot_required = False
        if self.medicament:
            product = self.medicament.name
            if (context.get('from_location') and context.get('from_location')):
                from_location = Location(context.get('from_location'))
                to_location = Location(context.get('to_location'))
                lot_required = product.lot_is_required(from_location,
                    to_location)
        self.product = product
        self.lot_required = lot_required


class CreatePrescriptionAndStockMove(Wizard):
    'Create Prescription Order and Stock Move'
    __name__ = 'gnuhealth.prescription.create'

    start = StateView('gnuhealth.prescription.create.start',
        'health_stock_hesm.view_create_prescription_start_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Confirm', 'confirm', 'tryton-ok', True),
            ])
    confirm = StateTransition()
    open_ = StateAction('health.action_gnuhealth_prescription_view')

    def transition_confirm(self):
        pool = Pool()
        Prescription = pool.get('gnuhealth.prescription.order')
        Patient = pool.get('gnuhealth.patient')
        PrescLine = pool.get('gnuhealth.prescription.line')
        StockMove = pool.get('stock.move')
        Lang = Pool().get('ir.lang')
        language, = Lang.search([
            ('code', '=', Transaction().language),
            ])
        Warning = pool.get('res.user.warning')

        #Check to not deliver medicament before end treatment date and expiration date
        lines = [[x.medicament,x.start_treatment] for x in self._get_prescription_lines()]

        patient_id = self.start.patient
        patient_presc = Prescription.search([('patient','=',patient_id)])
        patient_presc_line = PrescLine.search([('name','in',[x.id for x in patient_presc]),
                                               ('medicament','in',[x[0] for x in lines]),])
        # TODO fix with list comprehension
        #for x in patient_presc_line:
            #for y in lines:
                #warning_name = None
                #if x.medicament == y[0] and x.end_treatment and x.end_treatment > y[1]:
                    #warning_name = '/'.join([
                        #str(x.id), str(x.medicament.id), str(x.end_treatment)
                        #])
                #if Warning.check(warning_name):
                    #raise UserWarning(warning_name,
                            #'health_stock_hesm.stock_move_before_time',
                            #u'The medicament '+x.medicament.name.template.name
                            #+ ' is being delivered before end treatment date: '
                            #+ Lang.strftime(y[1], language.code, language.date)
                            #)

        # Create Prescription
        prescription = Prescription()
        prescription.patient = self.start.patient
        prescription.prescription_type = self.start.prescription_type
        prescription.has_insurance = self.start.has_insurance
        prescription.prescription_date = self.start.prescription_date
        prescription.healthprof = self.start.healthprof
        prescription.diagnosis = self.start.diagnosis
        prescription.pharmacy = self.start.pharmacy
        prescription.notes = self.start.notes
        prescription.prescription_line = self._get_prescription_lines()
        prescription.user_id = Transaction().user
        prescription.pregnancy_warning = self.start.pregnancy_warning
        prescription.pharmacovigilance_warning = (
            self.start.pharmacovigilance_warning)
        prescription.prescription_warning_ack = (
            self.start.prescription_warning_ack)
        prescription.state = 'draft'
        Prescription.save([prescription])
        Prescription.create_prescription([prescription])

        # Create Stock Moves
        output_location = self.start.output_location or self.start.to_location
        moves = []
        for line in self.start.lines:
            move = StockMove()
            move.origin = prescription
            move.effective_date = self.start.effective_date
            move.from_location = self.start.from_location
            move.to_location = output_location
            move.product = line.product
            move.lot = line.lot
            move.quantity = line.quantity
            move.uom = line.product.default_uom
            move.unit_price = line.product.list_price
            moves.append(move)
        StockMove.save(moves)
        StockMove.do(moves)

        if self.start.output_location:
            moves = []
            for line in self.start.lines:
                move = StockMove()
                move.origin = prescription
                move.effective_date = self.start.effective_date
                move.from_location = output_location
                move.to_location = self.start.to_location
                move.product = line.product
                move.lot = line.lot
                move.quantity = line.quantity
                move.uom = line.product.default_uom
                move.unit_price = line.product.list_price
                moves.append(move)
            StockMove.save(moves)
            StockMove.do(moves)

        self.start.prescription = prescription
        return 'open_'

    def _get_prescription_lines(self):
        pool = Pool()
        PrescriptionLine = pool.get('gnuhealth.prescription.line')

        lines = []
        for line in self.start.lines:
            prescription_line = PrescriptionLine()
            prescription_line.medicament = line.medicament
            prescription_line.quantity = line.quantity
            prescription_line.add_to_history = self.start.add_to_history
            prescription_line.start_treatment = self.start.prescription_date
            prescription_line.end_treatment = line.end_prescription_date
            prescription_line.indication = None
            lines.append(prescription_line)
        return lines

    def do_open_(self, action):
        action['pyson_domain'] = PYSONEncoder().encode([
            ('id', '=', self.start.prescription.id),
            ])
        action['name'] += ' (%s)' % (self.start.prescription.rec_name)
        return action, {}

    def transition_open_(self):
        return 'end'

    @classmethod
    def __setup__(cls):
        super(CreatePrescriptionAndStockMove, cls).__setup__()
        #cls._error_messages.update({
            #'stock_move_before_time':
                #'Delivering medicament before time',
            #'stock_move_expiraded_lot':
                #'Expired medication',
            #})
