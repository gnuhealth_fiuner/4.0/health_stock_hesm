# -*- coding: utf-8 -*-
# This file is part of health_stock_hesm module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from datetime import datetime, timedelta

from trytond.model import ModelView, fields
from trytond.wizard import (Wizard, StateView, Button, StateTransition,
    StateAction)
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Or, PYSONEncoder, Not
from trytond.transaction import Transaction
from trytond.exceptions import UserError, UserWarning


__all__ = ['CreateStockMove']


class CreateStockMove(Wizard):
    'Create Prescription Order and Stock Move'
    __name__ = 'gnuhealth.prescription.move.stock'

    start = StateView('gnuhealth.prescription.create.start',
        'health_stock_hesm.view_create_prescription_start_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Confirm', 'confirm', 'tryton-ok', True),
            ])
    confirm = StateTransition()
    #open_ = StateAction('health.action_gnuhealth_prescription_view')

    def default_start(self, fields):
        Prescription = Pool().get('gnuhealth.prescription.order')
        res = {}
        if len(Transaction().context['active_ids']) > 1:
            raise UserError('health_stock_hesm.more_than_one_prescription')
        prescription = Prescription(Transaction().context['active_ids'][0])
        if not prescription.prescription_line:
           raise UserError('health_stock_hesm.no_lines')

        '''Load only the difference between the medicament lines and stock moves'''
        #load all the medicament line products and quantities
        medicament_line = dict((str(line.medicament.name.id), line.quantity)
                               for line in prescription.prescription_line)
        #load all the product line stock total moves
        prescription_moves = [move for move in prescription.moves\
            if move.to_location.id == prescription.patient.name.customer_location.id]
        move_line = {}
        for move in prescription_moves:
            if str(move.product.id) in move_line:
                move_line[str(move.product.id)] += int(move.quantity)
            else:
                move_line[str(move.product.id)] = int(move.quantity)
        #make the difference between medicament line products and product line stock moves
        remainder_line = {}
        for med in medicament_line:
            remainder = medicament_line[med] - move_line[med] if med in move_line else medicament_line[med]
            if remainder:
                remainder_line[med] = remainder

        if not remainder_line:
            raise UserError('health_stock_hesm.no_more_remainder')

        #make the medicaments line to put on the start form
        medicament_product = dict((str(line.medicament.name.id), line.medicament.id)
                                  for line in prescription.prescription_line)
        lines = []
        for line in remainder_line:
            lines.append({
                'medicament': medicament_product[line],
                'quantity': remainder_line[line],
                'end_prescription_date': datetime.now()+timedelta(days=14),
                })
        res = {
            'patient': prescription.patient.id,
            'prescription_type': prescription.prescription_type,
            'has_insurance': prescription.has_insurance,
            'prescription_date': prescription.prescription_date,
            'healthprof': prescription.healthprof.id,
            'diagnosis': prescription.diagnosis and prescription.diagnosis.id or None,
            'pregnancy_warning': prescription.pregnancy_warning,
            'pharmacovigilance_warning': prescription.pharmacovigilance_warning,
            'prescription_warning_ack': prescription.prescription_warning_ack,
            'notes': prescription.notes,
            'lines': lines,
            'prescription_id': prescription.id,
            }
        return res

    def transition_confirm(self):
        pool = Pool()
        Prescription = pool.get('gnuhealth.prescription.order')
        Patient = pool.get('gnuhealth.patient')
        PrescLine = pool.get('gnuhealth.prescription.line')
        StockMove = pool.get('stock.move')
        Lang = Pool().get('ir.lang')
        language, = Lang.search([
            ('code', '=', Transaction().language),
            ])
        Warning = pool.get('res.user.warning')

        #Check to not deliver medicament before end treatment date and expiration date
        lines = [[x.medicament,x.start_treatment] for x in self._get_prescription_lines()]

        patient_id = self.start.patient
        patient_presc = Prescription.search([('patient','=',patient_id)])
        patient_presc_line = PrescLine.search([('name','in',[x.id for x in patient_presc]),
                                               ('medicament','in',[x[0] for x in lines]),])

        #for x in patient_presc_line:
            #for y in lines:
                #warning_name = None
                #if x.medicament == y[0] and x.end_treatment and x.end_treatment > y[1]:
                    ##warning_name = '/'.join([
                        ##str(x.id), str(x.medicament.id), str(x.end_treatment)
                        ##])
                    #warning_name = '22'
                #if Warning.check(warning_name):
                    ##date_warning = Lang.strftime(y[1],language.code, language.date)
                    ##date_warning = str(data_warning)
                    #raise UserWarning(warning_name,
                            #warning_name,
                            #warning_name)
                    #raise UserWarning(warning_name,
                            #'health_stock_hesm.stock_move_before_time',
                            #'The medicament '+x.medicament.name.template.name
                            #+ ' is being delivered before end treatment date: '
                            #+ str(x.end_treatment))

        # Create Stock Moves
        output_location = self.start.output_location or self.start.to_location
        moves = []

        for line in self.start.lines:
            move = StockMove()
            move.origin = self.start.prescription_id
            move.effective_date = self.start.effective_date
            move.from_location = self.start.from_location
            move.to_location = output_location
            move.product = line.product
            move.lot = line.lot
            move.quantity = line.quantity
            move.uom = line.product.default_uom
            move.unit_price = line.product.list_price
            moves.append(move)
        StockMove.save(moves)
        StockMove.do(moves)

        if self.start.output_location:
            moves = []
            for line in self.start.lines:
                move = StockMove()
                move.origin = prescription
                move.effective_date = self.start.effective_date
                move.from_location = output_location
                move.to_location = self.start.to_location
                move.product = line.product
                move.lot = line.lot
                move.quantity = line.quantity
                move.uom = line.product.default_uom
                move.unit_price = line.product.list_price
                moves.append(move)
            StockMove.save(moves)
            StockMove.do(moves)

        return 'end'

    def _get_prescription_lines(self):
        pool = Pool()
        PrescriptionLine = pool.get('gnuhealth.prescription.line')

        lines = []
        for line in self.start.lines:
            prescription_line = PrescriptionLine()
            prescription_line.medicament = line.medicament
            prescription_line.quantity = line.quantity
            prescription_line.add_to_history = self.start.add_to_history
            prescription_line.start_treatment = self.start.prescription_date
            prescription_line.end_treatment = line.end_prescription_date
            prescription_line.indication = None
            lines.append(prescription_line)
        return lines

    #def do_open_(self, action):
        #action['pyson_domain'] = PYSONEncoder().encode([
            #('id', '=', self.start.prescription.id),
            #])
        #action['name'] += ' (%s)' % (self.start.prescription.rec_name)
        #return action, {}

    #def transition_open_(self):
        #return 'end'

    #@classmethod
    #def __setup__(cls):
        #super(CreateStockMove, cls).__setup__()
