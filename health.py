# -*- coding: utf-8 -*-
# This file is part of health_stock_hesm module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.exceptions import UserError


class PatientPrescriptionOrder(metaclass=PoolMeta):
    __name__ = 'gnuhealth.prescription.order'

    _states = {'readonly': Eval('state') != 'draft'}
    _depends = ['state']

    prescription_type = fields.Selection([
        ('inpatient', 'Inpatient'),
        ('outpatient', 'Outpatient'),
        ], 'Type', sort=False,
        states=_states, depends=_depends)
    has_insurance = fields.Boolean('Has Insurance',
        states=_states, depends=_depends)
    diagnosis = fields.Many2One('gnuhealth.pathology', 'Diagnosis',
        states=_states, depends=_depends)
    pharmacovigilance_warning = fields.Boolean('Pharmacovigilance Warning',
        states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(PatientPrescriptionOrder, cls).__setup__()
        cls.pregnancy_warning.readonly = False
        cls.pregnancy_warning.states['readonly'] = True
        #cls._error_messages.update({
            #'drug_pharmacovigilance_warning': ('A medicament under '
                #'pharmacovigilance is being prescribed.\n\n'
                #'Verify and check for safety the prescribed drug.'),
            #})

    @staticmethod
    def default_prescription_type():
        return 'inpatient'

    @staticmethod
    def default_has_insurance():
        return False

    @classmethod
    def copy(cls, prescriptions, default=None):
        if default is None:
            default = {}
        default = default.copy()
        # default['diagnosis'] = None
        return super(PatientPrescriptionOrder, cls).copy(
            prescriptions, default=default)

    def check_prescription_warning(self):
        if not self.prescription_warning_ack:
            if self.pregnancy_warning:
                raise UserError('health_stock_hesm.drug_pregnancy_warning')
            if self.pharmacovigilance_warning:
                raise UserError('health_stock_hesm.drug_pharmacovigilance_warning')

    def on_change_patient(self):
        pass

    @fields.depends('patient')
    def on_change_with_pregnancy_warning(self):
        # Trigger the warning if the patient is at a childbearing age
        if self.patient and self.patient.childbearing_age:
            return True
        return False

    @fields.depends('pregnancy_warning')
    def on_change_pregnancy_warning(self):
        if self.pregnancy_warning:
            self.prescription_warning_ack = False

    @fields.depends('prescription_line')
    def on_change_with_pharmacovigilance_warning(self):
        # Trigger the warning if any medicament requires pharmacovigilance
        if self.prescription_line:
            for line in self.prescription_line:
                if (line.medicament and
                        line.medicament.pharmacovigilance_warning):
                    return True
        return False

    @fields.depends('pharmacovigilance_warning')
    def on_change_pharmacovigilance_warning(self):
        if self.pharmacovigilance_warning:
            self.prescription_warning_ack = False


class Medicament(metaclass=PoolMeta):
    __name__ = 'gnuhealth.medicament'

    pharmacovigilance_warning = fields.Boolean('Pharmacovigilance Warning',
        help='The drug is under pharmacovigilance')


class HealthInstitution(metaclass=PoolMeta):
    'Health Institution'
    __name__ = 'gnuhealth.institution'

    main_pharmacy = fields.Many2One('party.party', 'Main pharmacy',
                                     domain=[('is_pharmacy', '=', True)],)

    @fields.depends('name')
    def on_change_with_main_pharmacy(self):
        if self.name and self.name.is_pharmacy:
            return self.name.id

class PrescriptionLine(metaclass=PoolMeta):
    'Prescription Line'
    __name__ = 'gnuhealth.prescription.line'
